public class ArrayManipulation {
    public static void main(String[] args) {
        // Step 2: Declare and initialize arrays
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];

        // Step 3: Print elements of the "numbers" array using a for loop
        System.out.print("numbers array: ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();

        // Step 4: Print elements of the "names" array using a for-each loop
        System.out.print("names array: ");
        for (String name : names) {
            System.out.print(name + " ");
        }
        System.out.println();

        // Step 5: Initialize elements of the "values" array with any four decimal values
        values[0] = 1.5;
        values[1] = 2.7;
        values[2] = 3.2;
        values[3] = 4.9;

        // Step 6: Calculate and print the sum of all elements in the "numbers" array
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        System.out.println("Sum of numbers array: " + sum);

        // Step 7: Find and print the maximum value in the "values" array
        double max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("Maximum value in values array: " + max);

        // Step 8: Create and print the "reversedNames" array
        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[names.length - 1 - i] = names[i];
        }
        System.out.print("reversedNames array: ");
        for (String name : reversedNames) {
            System.out.print(name + " ");
        }
        System.out.println();

        // Step 9 (BONUS): Sort the "numbers" array in ascending order using a simple bubble sort
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - 1 - i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }

        // Print the sorted "numbers" array
        System.out.print("Sorted numbers array: ");
        for (int num : numbers) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}