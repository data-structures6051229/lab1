public class ArrayManipulation2 {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        duplicateZeros(numbers);
        printArray(numbers);
    }

    public static void duplicateZeros(int[] arr) {
        int length = arr.length;
        int shift = 0;

        // Count the number of zeros to be duplicated
        for (int num : arr) {
            if (num == 0) {
                shift++;
            }
        }

        // Iterate from the end of the original array
        for (int i = length - 1; i >= 0; i--) {
            // Skip elements that will be beyond the modified array length
            if (i + shift < length) {
                arr[i + shift] = arr[i];
            }

            // Duplicate zeros
            if (arr[i] == 0) {
                shift--;
                if (i + shift < length) {
                    arr[i + shift] = 0;
                }
            }
        }
    }

    public static void printArray(int[] arr) {
        System.out.print("Modified numbers array: ");
        for (int number : arr) {
            System.out.print(number + " ");
        }
        System.out.println();
    }
}